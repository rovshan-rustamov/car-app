package com.example.carapp.service;

import com.example.carapp.dto.CarDto;
import com.example.carapp.dto.UpdateCarDto;
import com.example.carapp.model.Car;
import com.example.carapp.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CarService {
    private final CarRepository carRepository;
    private final ModelMapper modelMapper;

    public void saveCar(CarDto dto){
        Car car = modelMapper.map(dto, Car.class);
        carRepository.save(car);
    }

    public void updateCar(UpdateCarDto dto){
        Optional<Car> entity = carRepository.findById(dto.getId());
        entity.ifPresent(car1 -> {
                car1.setColor(dto.getColor());
                carRepository.save(car1);
        });

    }

    public CarDto getCarById(Long id){
        Car car = carRepository.findById(id).get();
        CarDto carDto = modelMapper.map(car, CarDto.class);
        return carDto;
    }

    public List<CarDto> getAllCars(){
        List<Car> carList = carRepository.findAll();
        List<CarDto> carDtoList = carList
                .stream()
                .map(car -> modelMapper.map(car, CarDto.class))
                .collect(Collectors.toList());
        return carDtoList;
    }

    public String deleteCar(Long id){
        carRepository.deleteById(id);
        return "deleted";
    }
}
