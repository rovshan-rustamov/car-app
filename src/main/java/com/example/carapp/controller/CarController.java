package com.example.carapp.controller;

import com.example.carapp.dto.CarDto;
import com.example.carapp.dto.UpdateCarDto;
import com.example.carapp.model.Car;
import com.example.carapp.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cars")
@RequiredArgsConstructor
public class CarController {
    private final CarService carService;
    @PostMapping("/reg")
    public String registerACar(@RequestBody CarDto carDto){
        carService.saveCar(carDto);
        return "Registered";
    }

    @PutMapping("/update-car")
    public String updateCar(@RequestBody UpdateCarDto updateCarDto){
        carService.updateCar(updateCarDto);
        return "Updated the car";
    }
    @GetMapping("/{id}")
    public CarDto getCarById(@PathVariable Long id){
       return carService.getCarById(id);
    }

    @GetMapping
    public List<CarDto> getAllCars(){
        return carService.getAllCars();
    }
    @DeleteMapping
    public String deleteCar(@PathVariable Long id){
        carService.deleteCar(id);
        return "deleted";
    }




}
