package com.example.carapp.dto;

import lombok.Data;

@Data
public class UpdateCarDto {
    Long id;
    String color;
}
