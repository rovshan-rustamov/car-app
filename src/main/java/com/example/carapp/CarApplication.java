package com.example.carapp;

import com.example.carapp.model.Car;
import com.example.carapp.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class CarApplication implements CommandLineRunner {
    private final CarRepository carRepository;

    public static void main(String[] args) {
        SpringApplication.run(CarApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        Car car = new Car();
        car.setYear(2023);
        car.setModel("BMW");
        car.setEngine("SOMEEngine");
        car.setColor("Black");
        car.setMaker("NotAzerbaijan");
        carRepository.save(car);
    }
}
