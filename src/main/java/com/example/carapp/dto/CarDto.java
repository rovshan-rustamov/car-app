package com.example.carapp.dto;

import lombok.Data;

@Data
public class CarDto {
    Integer year;
    String model;
    String engine;
    String color;
    String maker;
}
